<?php

/**
 * Enqueue scripts and styles.
 */
add_action( 'wp_enqueue_scripts', 'theme_enqueue_styles' );
function theme_enqueue_styles() {
  	wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );
}

/**
 * Load Jetpack scripts.
 */
function blask_child_jetpack_scripts() {
	if ( is_category() || is_page_template( 'newsletter-page.php' ) ) {
		wp_enqueue_script( 'blask-portfolio', get_stylesheet_directory_uri() . '/js/portfolio.js', array( 'jquery', 'masonry' ), '20150624', true );
	}
}
add_action( 'wp_enqueue_scripts', 'blask_child_jetpack_scripts' );

/**
 * Remove Category from Archive title
 * https://wordpress.org/plugins/wp-remove-category-from-archive-title/
 */
add_filter( 'get_the_archive_title', 'blask_child_remove_category_title' );
function blask_child_remove_category_title( $title ) {
    if ( is_category() ) {
        $title = single_cat_title( '', false );
    }
    return $title;
}

/**
* Change text strings
*
* @link http://codex.wordpress.org/Plugin_API/Filter_Reference/gettext
*/
function blask_child_custom_related_products_text( $translated_text, $text, $domain ) {
  switch ( $translated_text ) {
    case 'Related products' :
      $translated_text = __( 'Related publications', 'woocommerce' );
      break;
  }
  return $translated_text;
}
add_filter( 'gettext', 'blask_child_custom_related_products_text', 20, 3 );

?>