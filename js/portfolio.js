( function( $ ) {

	$( window ).load( function() {

		var portfolio_wrapper = $( '.portfolio-wrapper' );

		portfolio_wrapper.imagesLoaded( function() {
			if ( $( 'body' ).hasClass( 'rtl' ) ) {
				portfolio_wrapper.masonry( {
					columnWidth: '.post',
					itemSelector: '.post',
					transitionDuration: 0,
					isOriginLeft: false,
					gutter: 40,
				} );
			} else {
				portfolio_wrapper.masonry( {
					columnWidth: '.post',
					itemSelector: '.post',
					transitionDuration: 0,
					gutter: 40,
				} );
			}

			// Show the blocks
			$( '.post' ).animate( {
				'opacity' : 1
			}, 500 );
		} );

		$( window ).resize( function () {

			// Force layout correction after 1500 milliseconds
			setTimeout( function () {
				portfolio_wrapper.masonry({
					columnWidth: '.post',
					itemSelector: '.post',
					transitionDuration: 0,
					gutter: 40,
				});
			}, 1500 );

		} );

		// Layout posts that arrive via infinite scroll
		$( document.body ).on( 'post-load', function () {

			var new_items = $( '.infinite-wrap .post' );

			portfolio_wrapper.append( new_items );
			portfolio_wrapper.masonry( 'appended', new_items );

			// Force layout correction after 1500 milliseconds
			setTimeout( function () {

				portfolio_wrapper.masonry();

				// Show the blocks
				$( '.post' ).animate( {
					'opacity' : 1
				}, 250 );

			}, 1500 );

		} );

	} );

} )( jQuery );
